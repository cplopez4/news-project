import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import { CronJob } from "cron";
import { Routes } from "./routes/articles";
import newsTask from "./tasks/news";

class App {

    public app: express.Application;
    public articlesRoutes: Routes = new Routes();
    public cronJob: CronJob;

    public mongoUrl: string = 'mongodb://localhost/articles'; 

    constructor() {
        this.app = express();
        this.config();
        this.articlesRoutes.routes(this.app);
        this.mongoSetup();
        
        // CronJob to trigger 'newsTask' every 1 hour
        this.cronJob = new CronJob('0 * * * *', newsTask, null, true, 'America/Santiago', {}, true);
    }

    private mongoSetup(): void {
        mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl);    
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

}

export default new App().app;