import { Request, Response } from "express";
import { ArticlesController } from "../controllers/articlesController";

export class Routes {

    public articlesController: ArticlesController = new ArticlesController() 
    
    public routes(app): void {   
        
        app.route('/')
        .get((req: Request, res: Response) => {            
            res.status(200).send({
                message: 'Node.js Articles API'
            })
        })
        

        // Articles Routes

        app.route('/articles')
        .get(this.articlesController.getArticles)        
        .post(this.articlesController.addNewArticle);

        // Article detail
        app.route('/articles/:articleId')
        .get(this.articlesController.getArticleByID)
        .put(this.articlesController.updateArticle)
        .delete(this.articlesController.deleteArticle)

        //These methods could be put together in one call with a binary param that defines the wanted status. This is the proposed solution for consistency (like/dislike behaviour). 
        // **This kind of methods must have a security layer like an api token**
        
        // Active article
        app.route('/articles/:articleId/active')
        .get(this.articlesController.changeArticleToActive)

        // Inactive article
        app.route('/articles/:articleId/inactive')
        .get(this.articlesController.changeArticleToInactive)
    }
}