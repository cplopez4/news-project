import * as mongoose from 'mongoose';
import * as request from 'request';
import { ArticleSchema } from '../models/article';

const Article = mongoose.model('Article', ArticleSchema);
const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

export default function () {
    request(url, { json: true }, (err, res, body) => {
        if(err) { return console.log(err) }

        if(body.hits && body.hits.length > 0){
            let options = { upsert: true, new: true, setDefaultsOnInsert: true };

            for (let article of body.hits) {
                if(article.story_title || article.title){
                    // Validates if Article with article.ObjectID already exists (upsert)
                    Article.findOneAndUpdate({ story_id: article.story_id }, article, options, (err, insertedArticle) => {
                        if(err){
                            return console.log(err);
                        }    
                        console.log("Processed Article with ObjectID: " + insertedArticle.objectID);
                    })
                }
                else { return console.log("Article "+ article.objectID +" doesn't have a title!") }
            }
        }
        
        else { return console.log("Results are empty!") }
    })
}