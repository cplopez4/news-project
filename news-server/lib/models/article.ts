import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const ArticleSchema = new Schema({
    created_at: {  
        type: Date,
        default: Date.now 
    },
    active: {  
        type: Boolean,
        default: true
    },
    title: {  
        type: String
    },
    url: {  
        type: String
    },
    author: {  
        type: String
    },
    points: {  
        type: Number
    },
    story_text: {  
        type: String
    },
    comment_text: {  
        type: String
    },
    num_comments: {  
        type: Number
    },
    story_id: {  
        type: Number
    },
    story_title: {  
        type: String
    },
    story_url: {  
        type: String
    },
    parent_id: {  
        type: Number
    },
    created_at_i: {  
        type: Number
    },
    _tags: {  
        type: Array
    },
    objectID: {  
        type: String
    },
    _highlightResult: {  
        type: Object
    }
});