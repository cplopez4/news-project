News Project for ReignDesign
=========================================

This repository contains server & client side code using `TypeScript` language.


# Running Server and Client locally
## Prerequisites

First, ensure you have the following installed:

1. NodeJS - Download and Install latest version of Node: [NodeJS](https://nodejs.org). This project is built with `v8.12.0`.
2. Git - Download and Install [Git](https://git-scm.com)
3. Angular CLI - Install Command Line Interface for Angular [https://cli.angular.io/](https://cli.angular.io/).
4. MongoDB running in default port (27017) - [Install Comunnity Edition](https://docs.mongodb.com/manual/installation/).


## Clone repository

In order to start the project use:

```bash
$ git clone git@bitbucket.org:cplopez4/news-project.git
$ cd news-project
```
There are two separate client and server projects for each layer, so they have to run at the same time.

## Run Server

To run server locally, just install dependencies and run script:

```bash
$ cd news-server
$ npm install -g gulp-cli
$ npm install
$ gulp build
$ npm start
```
You could also run `npm run prod` and create a build without `gulp` to then run the server, or `npm run dev` to watch directly from the typescript files.

The `Express News API` server will be running on port `3000`.

For the persistency of data in MongoDB, check that mongod service is up and running:

```bash
$ sudo service mongod start
```

## Run Angular Client

Open other command line window and run following commands:

```bash
$ cd news-app
$ npm install
$ npm start
```
`npm start` serves the app with a proxy, configured in `proxy.conf.json` to consume server endpoints without CORS issues.

Now open your browser in following URL: [http://localhost:4200](http://localhost:4200/).

Every first time you start the server, the `CronJob` task populates the MongoDB collection `articles` from the remote News API. Then, it updates the DB every 1 hour.