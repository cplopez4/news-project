import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  private baseUrl: string = environment.apiUrl;
  private resourceUrl_URL: string = "/articles/"

  constructor(private http: HttpClient) { }

  // Get all articles from the API
  getAllArticles() {
    return this.http.get(this.baseUrl + this.resourceUrl_URL);
  }

  // Change state of article to inactive for removal of view
  changeToInactive(id: string) {
    return this.http.get(this.baseUrl + this.resourceUrl_URL + id + "/inactive");
  }
}
