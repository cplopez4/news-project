import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';
import * as moment from 'moment';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {

  articles: any = [];

  constructor(private articlesService: ArticlesService) { }

  ngOnInit() {
    this.articlesService.getAllArticles().subscribe(articles => {
      this.articles = articles;

      for(let a of this.articles){
        a.created_at_formatted = this.formatDate(a.created_at);
      }
    });
  }

  remove(article: any, event: Event) {
    this.articlesService.changeToInactive(article._id).subscribe(changedArticle => {
      this.articles.find(elem => elem.story_id == article.story_id).active = false;
    });
    
    event.preventDefault();
  }

  formatDate(date: Date) {
    if(moment(date).isSame(Date.now(), 'day')){
      return moment(date).format('hh:mm a');
    }
    else if(moment(date).isSame(moment().subtract(1, 'day').toDate(), 'day')){
      return "Yesterday";
    }
    else {
      return moment(date).format('MMM D');
    }
  }
}
